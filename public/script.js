function getAndUpdate(){
    console.log("Updating List...");
    tit = document.getElementById('title').value;
    desc = document.getElementById('description').value;

    document.getElementById('title').value = "";
    document.getElementById('description').value = "";

    if (localStorage.getItem('itemsJson')==null){
        itemJsonArray = [];
        itemJsonArray.push([tit, desc]);
        localStorage.setItem('itemsJson', JSON.stringify(itemJsonArray))
    }
    else{
        itemJsonArrayStr = localStorage.getItem('itemsJson')
        itemJsonArray = JSON.parse(itemJsonArrayStr);
        itemJsonArray.push([tit, desc]);
        localStorage.setItem('itemsJson', JSON.stringify(itemJsonArray))
    }
    update();
}

function update(){
    if (localStorage.getItem('itemsJson')==null){
        itemJsonArray = []; 
        localStorage.setItem('itemsJson', JSON.stringify(itemJsonArray))
    } 
    else{
        itemJsonArrayStr = localStorage.getItem('itemsJson')
        itemJsonArray = JSON.parse(itemJsonArrayStr); 
    }
    // Populate the table
    let tableBody = document.getElementById("tableBody");
    let str = "";
    itemJsonArray.forEach((element, index) => {
        str += `
        <tr>
        <th scope="row">${index + 1}</th>
        <td>${element[0]}</td>
        <td>${element[1]}</td> 
        <td><button class="btn btn-sm btn-primary" onclick="deleted(${index})">Delete</button></td> 
        <td><button class="btn btn-sm btn-primary" onclick="edit(${index})">Edit</button></td> 
        </tr>`; 
    });
    tableBody.innerHTML = str;
}

add = document.getElementById("add");
add.addEventListener("click", getAndUpdate);
update();

function deleted(itemIndex){
    console.log("Delete", itemIndex);
    itemJsonArrayStr = localStorage.getItem('itemsJson')
    itemJsonArray = JSON.parse(itemJsonArrayStr);

    itemJsonArray.splice(itemIndex, 1);
    localStorage.setItem('itemsJson', JSON.stringify(itemJsonArray));
    update();
}


function edit(itemIndex) {

    let newTitle = prompt("Enter new title:");
    let newDescription = prompt("Enter new description:");

    // Call the edit function with the new data
    editItem(itemIndex, newTitle, newDescription);
}

function edit(itemIndex) {
    // Show the edit pop-up
    document.getElementById("editPopup").style.display = "block";

    // Set up the confirmEdit function with the itemIndex
    window.confirmEdit = function () {
        let newTitle = document.getElementById("newTitle").value;
        let newDescription = document.getElementById("newDescription").value;

        // Call the edit function with the new data
        editItem(itemIndex, newTitle, newDescription);

        // Hide the edit pop-up
        closeEditPopup();
    };

    // Set up the closeEditPopup function
    window.closeEditPopup = function () {
        document.getElementById("editPopup").style.display = "none";
        // Remove the confirmEdit and closeEditPopup functions to avoid conflicts
        delete window.confirmEdit;
        delete window.closeEditPopup;
    };

    document.getElementById('newTitle').value = "";
    document.getElementById('newDescription').value = "";

}

function editItem(itemIndex, newTitle, newDescription) {
    console.log("Edit", itemIndex, newTitle, newDescription);
    itemJsonArrayStr = localStorage.getItem('itemsJson');
    itemJsonArray = JSON.parse(itemJsonArrayStr);

    // Assuming each item is an array with title at index 0 and description at index 1.
    itemJsonArray[itemIndex][0] = newTitle;
    itemJsonArray[itemIndex][1] = newDescription;

    localStorage.setItem('itemsJson', JSON.stringify(itemJsonArray));
    update();
}


function openPopup() {
    document.getElementById("popup").style.display = "block";
}

function closePopup() {
    document.getElementById("popup").style.display = "none";
}

document.getElementById("clear").addEventListener("click", openPopup);

document.getElementById("closePopup").addEventListener("click", closePopup);

document.getElementById("confirmClear").addEventListener("click", function () {
    console.log('Clearing the storage');
    localStorage.clear();
    update(); 
    closePopup();
    
});